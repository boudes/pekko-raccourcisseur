package fr.mipn.raccourcisseur 
import org.apache.pekko 
import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import spray.json.JsString
import spray.json.JsValue
import spray.json.JsonFormat

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat
import spray.json.deserializationError

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import fr.mipn.raccourcisseur.UrlDB
// import fr.mipn.raccourcisseur.UrlDB._
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success

import org.apache.pekko.http.scaladsl.server.Directives._
import org.openapitools.server.model.Raccourci

/* Important: until I fully understand what's happening here (dependent types vs threads, implicit context, cosmic rays), 
   do not attempt to define these abstracts methods inherited from the Api inside Main, or your HTTP requests will be stuck */
class Api(system: ActorSystem[UrlDB.Command], urldb: ActorRef[UrlDB.Command]) extends LienCourtApiService {
  implicit val timeout: Timeout = 3.seconds
  implicit val actorsystem: ActorSystem[_] = system
  implicit val executionContext: ExecutionContext = system.executionContext

  /* We know that: type Route = RequestContext => Future[RouteResult]
     And that a StandardRoute is a Route that can be implicitly converted into a Directive (fitting any signature).
     we use our trick of transforming a Future[Something => Future[OtherThing]] into a Something => Future[OtherThing] to deal
     with the fact that the content of our responses are coming from a certain Future */ 
  def routeFromFutureRoute(futureroute: Future[Route]): Route = (context) => futureroute.flatMap(route => route(context)) 

  import pekko.http.scaladsl.model.headers._

  override def createShortLink(nom: String, url: String)(using toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route = routeFromFutureRoute {
      urldb.ask(ref => UrlDB.Create(nom, url, ref)).map { ourl =>
        (ourl match {
          case Some(raccourci) => complete(200, raccourci.asInstanceOf[Raccourci])
          case None => complete((401, ""))
        })
      }    
    }
  override def shortLink(nom: String): Route = routeFromFutureRoute {
    urldb.ask(ref => UrlDB.Get(nom, ref)).map {
      case Some(url) => respondWithHeaders(List(RawHeader("Location", url.toString))) {
            complete(302, s"redirection vers : $url")
          }
      case None => complete(404, s"$nom Not Found")
    }
  }
  override def editShortLink(raccourci: Raccourci)
  (using toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route = routeFromFutureRoute {
    urldb.ask(ref => UrlDB.Update(raccourci, ref)).map { ourl =>
      (ourl match {
        case Some(raccourci) => complete(200, raccourci.asInstanceOf[Raccourci])
        case None => complete((401, ""))
      })
    }    
  }

  override def adminListLinks(key: String)
  (implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Seq[Raccourci]]): Route = routeFromFutureRoute {
    system.ask(ref => UrlDB.ListUrls(key, ref)).map { ourls =>
      (ourls match {
        case Some(raccourcis) => complete(200, raccourcis.asInstanceOf[Seq[Raccourci]])
        case None => complete((401, ""))
      })
    } 
  }
}

@main def main: Unit = {
  // needed to run the route
  implicit val system: ActorSystem[UrlDB.Command] = ActorSystem(UrlDB(), "raccourcisseur")
  given ExecutionContext = system.executionContext
  
  system.log.info(s"\n\n****** Raccourcisseur *******\n")

  object Marsh extends LienCourtApiMarshaller {
      implicit val r : RootJsonFormat[Raccourci] = jsonFormat3(Raccourci.apply)
      override implicit def fromEntityUnmarshallerRaccourci: FromEntityUnmarshaller[Raccourci] = r
      override implicit def toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci] = r

      override implicit def toEntityMarshallerRaccourciarray: ToEntityMarshaller[Seq[Raccourci]] = immSeqFormat(r)
  }

  val api = new LienCourtApi(new Api(system, system), Marsh)

  val host = "localhost"
  val port = 8080

  val bindingFuture = Http().newServerAt(host, port).bind(api.route)
  println(s"Server online at http://${host}:${port}/\nPress RETURN to stop...")

  bindingFuture.failed.foreach { ex =>
    println(s"${ex} Failed to bind to ${host}:${port}!")
  }

  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}
