package fr.mipn.raccourcisseur

import org.apache.pekko

import pekko.actor.typed.{ActorRef, Behavior,ActorSystem}
import pekko.actor.typed.scaladsl.{Behaviors,AbstractBehavior,ActorContext}
import scala.util.Random

import org.openapitools.server.model.Raccourci

/* Our short links database is just an Actor named UrlDB */

/* the companion object hold the protocole UrlDB.Command actors should comply with while sending message to UrlDB's actors and 
   the apply method for instanciating nicely new UrlDB */
object UrlDB {
  sealed trait Command
  case class Create(targetUrl: String, targetName: String, replyTo: ActorRef[Option[Raccourci]]) extends Command
  case class Update(raccourci: Raccourci, replyTo: ActorRef[Option[Raccourci]]) extends Command
  case class ListUrls(key: String, replyTo: ActorRef[Option[List[Raccourci]]]) extends Command
  case class Get(name: String, replyTo: ActorRef[Option[String]]) extends Command 
  def apply(): Behavior[Command] =
    Behaviors.setup(context => new UrlDB(context))
}

/* UrlDB uses a Map urls to store the associations of strings with urls, messages from the UrlDB.Command protocole 
   triggers actions updating that map or getting informations from it. */ 
class UrlDB(context: ActorContext[UrlDB.Command]) extends AbstractBehavior[UrlDB.Command](context) {
  context.log.info("Hello UrlDB !")
  import UrlDB._

  var urls: Map[String, (String, String)] = Map("mipn" -> ("https://matt.mipn.fr", Random.alphanumeric.take(8).mkString))
  /* The random key stored within the Map allows to modifiy the association */

  var adminKey = Random.alphanumeric.take(30).mkString /* The adminkey allows to change any url */
  context.log.info(s"Clé d'administration : $adminKey") /* lets show it to the admin            */
  override def onMessage(msg: Command): Behavior[Command] =
    msg match {
    case Create(name, url, replyto) => // someone asked for the creation of a redirection
      replyto ! {
        context.log.info(s"Créer $name -> $url …")
        if (urls.isDefinedAt(name)) { // but that link is already used
          context.log.info(s"Erreur nom $name déjà défini")
          None
        }
        else { // this is a new one, lets answer with a secret key allowing to change it later
          context.log.info(s"Ajout de $name --> $url")
          val key = Random.alphanumeric.take(8).mkString
          urls += ((name, (url, key)))
          context.log.info(urls.toString)
          Option(Raccourci(name, url, key))
        }
      }
      this
    case Update(Raccourci(name, url, key), replyto) => // someone has a secret key and wants to update the link
      replyto ! {
        context.log.info(s"Créer $name -> $url …")
        if (!urls.isDefinedAt(name)) { // but that link is not defined
          context.log.info(s"Erreur nom $name non défini")
          None
        } else {
          if (key != urls(name)._2) {// …or the provided key is invalid for that link 
            None
          } else { // lets update the database
            context.log.info(s"modification de $name --> $url")
            urls = urls.updated(name, (url, key))
            context.log.info(urls.toString)
            Option(Raccourci(name, url, key))
          }
        }
      }
      this
    case Get(name, replyto) => // someone wants to access a link, lets get the url
      val url = urls.get(name).map(_._1)
      context.log.info(s"${name} -> ${url}")
      replyto ! url
      this
    case ListUrls(key, replyTo) => // the admin wants to list all the links
      val response = if (key != adminKey) {
        None
      } else {
        Some(urls.toList.map {
          case (name, (url, key)) => Raccourci(name, url, key)
        })
      }
      replyTo ! response
      this
   }
}

