package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.Raccourci


class LienCourtApi(
    lienCourtService: LienCourtApiService,
    lienCourtMarshaller: LienCourtApiMarshaller
) {

  
  import lienCourtMarshaller._

  lazy val route: Route =
    path("admin") { 
      get { 
        parameters("key".as[String]) { (key) => 
            lienCourtService.adminListLinks(key = key)
        }
      }
    } ~
    path("create") { 
      post { 
        parameters("nom".as[String], "url".as[String]) { (nom, url) => 
            lienCourtService.createShortLink(nom = nom, url = url)
        }
      }
    } ~
    path("edit") { 
      post {  
            entity(as[Raccourci]){ raccourci =>
              lienCourtService.editShortLink(raccourci = raccourci)
            }
      }
    } ~
    path(Segment) { (nom) => 
      get {  
            lienCourtService.shortLink(nom = nom)
      }
    }
}


trait LienCourtApiService {

  def adminListLinks200(responseRaccourciarray: Seq[Raccourci])(implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Seq[Raccourci]]): Route =
    complete((200, responseRaccourciarray))
  /**
   * Code: 200, Message: liste communiquée, DataType: Seq[Raccourci]
   */
  def adminListLinks(key: String)
      (implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Seq[Raccourci]]): Route

  def createShortLink200(responseRaccourci: Raccourci)(implicit toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route =
    complete((200, responseRaccourci))
  def createShortLink401: Route =
    complete((401, "une association utilise ce nom"))
  /**
   * Code: 200, Message: création effectuée, DataType: Raccourci
   * Code: 401, Message: une association utilise ce nom
   */
  def createShortLink(nom: String, url: String)
      (implicit toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route

  def editShortLink200(responseRaccourci: Raccourci)(implicit toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route =
    complete((200, responseRaccourci))
  def editShortLink401: Route =
    complete((401, "une association utilise ce nom"))
  def editShortLink403: Route =
    complete((403, "accès refusé"))
  /**
   * Code: 200, Message: création effectuée, DataType: Raccourci
   * Code: 401, Message: une association utilise ce nom
   * Code: 403, Message: accès refusé
   */
  def editShortLink(raccourci: Raccourci)
      (implicit toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]): Route

  def shortLink302: Route =
    complete((302, "redirection vers l&#39;url"))
  def shortLink404: Route =
    complete((404, "le nom n&#39;est pas associé"))
  def shortLink500: Route =
    complete((500, "erreur interne du serveur"))
  /**
   * Code: 302, Message: redirection vers l&#39;url
   * Code: 404, Message: le nom n&#39;est pas associé
   * Code: 500, Message: erreur interne du serveur
   */
  def shortLink(nom: String): Route

}

trait LienCourtApiMarshaller {
  implicit def fromEntityUnmarshallerRaccourci: FromEntityUnmarshaller[Raccourci]



  implicit def toEntityMarshallerRaccourciarray: ToEntityMarshaller[Seq[Raccourci]]

  implicit def toEntityMarshallerRaccourci: ToEntityMarshaller[Raccourci]

}

