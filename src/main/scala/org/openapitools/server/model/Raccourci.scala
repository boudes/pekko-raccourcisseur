package org.openapitools.server.model


/**
 * @param nom  for example: ''mipn''
 * @param url  for example: ''https://www.youtube.com/watch?v=dQw4w9WgXcQ''
 * @param key  for example: ''VwgzRZ2cnL''
*/
final case class Raccourci (
  nom: String,
  url: String,
  key: String
)

